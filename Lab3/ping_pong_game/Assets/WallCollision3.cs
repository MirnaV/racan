using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallCollision3 : MonoBehaviour
{
    // Start is called before the first frame update
    public Vector3 target;

    void Start()
    {
        target = new Vector3(-0.01f, 0.469f, -4.941f);
    }

    void Update()
    {
        transform.position = Vector3.MoveTowards(transform.position, target, 0.5f);
    }
}
