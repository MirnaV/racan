using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public Vector3 target;
    // Start is called before the first frame update
    void Start()
    {
       target = new Vector3(-4.287f, 0.4938f, 0.2f); 
    }

    // Update is called once per frame
    void Update()
    {
        float y_position = transform.position.y;
        if ((y_position - 0.4938f) > 0.1) {
            transform.position = Vector3.MoveTowards(transform.position, target, 0.5f);
        }
    }
}
