using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;


public class RandomMovement : MonoBehaviour
{

    public float pushForce = 10;
    public float speed1 = 0.1f;
    public float speed_increase = 0.001f;
    public Vector3 target;
    public Vector3 position;
    public float small_change = 0.001f;
    // Start is called before the first frame update
    void Start()
    {
        //System.Threading.Thread.Sleep(10000);
        target = new Vector3(0f, 0.4938f, 0f);
        position = new Vector3(0.1f, 0f, 0f);
        
        //rb.AddForce(new Vector3(10, 0, 0) * pushForce);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        float y_position = transform.position.y;
        Vector3 player_position = GameObject.Find("Capsule").transform.position;
        
        if (transform.position.x < -4.287 && transform.position.z < 2.5 && transform.position.z > -2.5) {
            GetComponent<Animator>().enabled = false;
        }

        if (transform.position.x > 4.4) {
            position.x *= (-1);
        } else if (transform.position.x < -4.3) {
            position.x *= (-1);
        } 
        if (transform.position.z > 3.9) {
            small_change *= (-1);
        } else if (transform.position.z < -4.1) {
            small_change *= (-1);
        } 
        
        if (System.Math.Abs(player_position.z - transform.position.z) < 0.4 && transform.position.x < player_position.x+0.5f ) {
            position.x *= (-1);
            small_change *= (-1);
        }
        
        if ((y_position - 0.4938f) > 0.1) {
            transform.position = Vector3.MoveTowards(transform.position, target, 0.5f);
        } else {
            Vector3 target_position = new Vector3(transform.position.x+position.x, transform.position.y+position.y, transform.position.z+position.z+small_change);
            transform.position = Vector3.MoveTowards(transform.position, target_position, speed1);
            speed1 += speed_increase;
        }
  
    }
    
    void OnCollisionEnter(Collision collision) {
        position.x = position.x * (-1);
    }
    
}
