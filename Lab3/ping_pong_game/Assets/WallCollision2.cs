using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallCollision2 : MonoBehaviour
{
    // Start is called before the first frame update
    public Vector3 target;

    void Start()
    {
        target = new Vector3(-5.52f, 0.469f, 0.07f);
    }

    void Update()
    {
        transform.position = Vector3.MoveTowards(transform.position, target, 0.5f);
    }
}
