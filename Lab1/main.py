import time

from OpenGL.GLU import *
from OpenGL.GLUT import *
from numpy import *
from load_obj_file import *
import spline
import numpy as np


def showScreen(point, fi, o_s, e_os):
    for i in range(len(points_to_draw) - 3):
        r_list = points_to_draw[i:i+4]
        for t in range(0, 99):
            glBegin(GL_LINES)
            point1 = spline.calculate_spline_point(t / 100, r_list)
            point2 = spline.calculate_spline_point((t+1)/100, r_list)
            glVertex3f(point1[0], point1[1], point1[2])
            glVertex3f(point2[0], point2[1], point2[2])
            glEnd()

    glPushMatrix()
    glTranslate(point[0], point[1], point[2])
    glRotatef(fi, o_s[0], o_s[1], o_s[2])
    glCallList(obj.gl_list)
    glPopMatrix()

    glutSwapBuffers()
    time.sleep(0.00001)


def some_function():
    for i in range(len(points_to_draw) - 3):
        r_list = points_to_draw[i:i+4]
        for t in range(0, 100):
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
            glLoadIdentity()
            gluLookAt(0.0, 10.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0)
            glTranslate(0, -20, 0)

            e_os = spline.calculate_tangent_point(t, r_list)
            e_os = e_os[0:3]
            o_s = np.cross(e_os, s_os)
            fi = np.degrees(np.arccos(np.dot(s_os, e_os) / (np.linalg.norm(e_os)))) / 2
            point = spline.calculate_spline_point(t/100, r_list)
            showScreen(point, fi, o_s, e_os)


viewport = (500, 500)
glutInit()
glutInitDisplayMode(GLUT_RGBA)
glutInitWindowSize(500, 500)
glutInitWindowPosition(0, 0)
wind = glutCreateWindow("OpenGL Coding Practice")
glutDisplayFunc(some_function)
glutIdleFunc(some_function)
glEnable(GL_DEPTH_TEST)
obj = OBJ("aircraft747.obj")
glMatrixMode(GL_PROJECTION)
glLoadIdentity()
width, height = viewport
gluPerspective(45.0, width / float(height), 1, 100.0)
glEnable(GL_DEPTH_TEST)
glMatrixMode(GL_MODELVIEW)

points_to_draw = []

with open("points.txt", "r") as f:
    lines = f.readlines()
    for line in lines:
        values = line.split(" ")
        values = list(map(float, values))
        values = list(x / 5 for x in values)

        points_to_draw.append(values)

s_os = np.array([0, 0, 1])

glutMainLoop()

