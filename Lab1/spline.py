import numpy as np

B = np.array([[-1, 3, -3, 1], [3, -6, 3, 0], [-3, 0, 3, 0], [1, 4, 1, 0]])
B2 = np.array([[-1, 3, -3, 1], [2, -4, 2, 0], [-1, 0, 1, 0]])


def calculate_spline_point(t, r_list):
    t_vector = np.array([t**3, t**2, t, 1]) * (1/6)
    r_vector = np.array(r_list)
    return np.matmul(np.matmul(t_vector, B),  r_vector)


def calculate_tangent_point(t, r_list):
    t_vector = np.array([t**2, t, 1]) / (1/2)
    r_vector = np.array(r_list)
    return np.matmul(np.matmul(t_vector, B2), r_vector)
