import os
import pygame
from OpenGL.GL import *


class OBJ:
    def __init__(self, filename):
        self.vertices = []
        self.faces = []

        for line in open(filename, "r"):
            # Check if it is line with data
            if not (line.startswith("v") or line.startswith("f")):
                continue

            # Check if it's an empty line
            values = line.split()
            if not values:
                continue

            if values[0] == "v":
                v = list(map(float, values[1:4]))
                self.vertices.append(v)
            elif values[0] == "f":
                f = list(map(int, values[1:4]))
                self.faces.append(f)

        # Creating a list of events that will be called in the main function to draw the object
        self.gl_list = glGenLists(1)
        glNewList(self.gl_list, GL_COMPILE)
        for face in self.faces:
            glBegin(GL_LINE_LOOP)
            for i in range(len(face)):
                glVertex3f(*self.vertices[face[i] - 1])
            glEnd()
        glEndList()

