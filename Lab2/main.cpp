#include <stdio.h>
#include <stdlib.h>

#include <vector>
#include <algorithm>

#include <GL/glew.h>

#include <GLFW/glfw3.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/norm.hpp>
#include <GL/freeglut.h>
#include <GL/glut.h>
#include <iostream>
#include <time.h>
#include <unistd.h>
#include <SOIL.h>

using namespace std;
using namespace glm;

struct Particle {
    vec3 position, speed;
    float r, g, b, a;
    float size, angle, weight;
    float lifetime = -0.1;
    float cameradistance;

    bool operator<(const Particle& that) const {
        return this->cameradistance > that.cameradistance;
    }
};

const int MaxParticles = 50;
Particle ParticleContainer[MaxParticles];
int LastUsedParticle = 0;
static GLfloat *g_particule_position_size_data = new GLfloat[MaxParticles*4];
static GLfloat *g_particule_color_data = new GLfloat[MaxParticles*4];

int FindUnusedParticle() {
    for (int i = LastUsedParticle; i < MaxParticles; i++) {
        if (ParticleContainer[i].lifetime < 0) {
            LastUsedParticle = i;
            return i;
        }
    }

    for (int i = 0; i < LastUsedParticle; i++) {
        if (ParticleContainer[i].lifetime < 0) {
            LastUsedParticle = i;
            return i;
        }
    }

    return -1;
}

void SortParticles() {
    sort(&ParticleContainer[0], &ParticleContainer[MaxParticles]);
}


void Display() {
    srand(time(NULL));
    GLuint texture;
    texture = SOIL_load_OGL_texture ("./explosion.bmp", SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_MIPMAPS | SOIL_FLAG_INVERT_Y | SOIL_FLAG_NTSC_SAFE_RGB | SOIL_FLAG_COMPRESS_TO_DXT);
    do {
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        double delta = 0.1;
        int newparticles = (int)(rand()%15);

        for (int i = 0; i < newparticles; i++) {
            int particleIndex = FindUnusedParticle();
            if (particleIndex == -1) {
                break;
            }
            ParticleContainer[particleIndex].lifetime = (float)(rand()%7);
            ParticleContainer[particleIndex].position = vec3(0.0f, 0.0f, 0.0f);


            float spread = 5.5f;
            vec3 maindir = vec3(0.0f, 10.0f, 0.0f);
            vec3 randomdir = vec3(
                (rand()%2000 - 1000.0f)/1000.0f,
				(rand()%2000 - 1000.0f)/1000.0f,
				(rand()%2000 - 1000.0f)/1000.0f
            );
            ParticleContainer[particleIndex].speed = maindir + randomdir*spread;
            ParticleContainer[particleIndex].r = (rand()%256)/1000.0f;
            ParticleContainer[particleIndex].g = (rand()%256)/1000.0f;
            ParticleContainer[particleIndex].b = (rand()%256)/1000.0f;
            ParticleContainer[particleIndex].a = ((rand()%256) / 3);
            ParticleContainer[particleIndex].size = (rand()%1000)/2000.0f + 0.1f;

        }
        int ParticlesCount = 0;

        for (int i = 0; i < MaxParticles; i++) {
            Particle &p = ParticleContainer[i];

            if (p.lifetime >= 0.0f) {
                p.lifetime -= delta;

                if (p.lifetime > 0.0f) {
                    p.speed += vec3(0.0f, -9.18f, 0.0f) * (float)delta * 0.5f;
                    p.position += p.speed * (float)delta;
                    
                    g_particule_position_size_data[4*ParticlesCount+0] = p.position.x;
					g_particule_position_size_data[4*ParticlesCount+1] = p.position.y;
					g_particule_position_size_data[4*ParticlesCount+2] = p.position.z;
												   
					g_particule_position_size_data[4*ParticlesCount+3] = p.size;

					g_particule_color_data[4*ParticlesCount+0] = p.r;
					g_particule_color_data[4*ParticlesCount+1] = p.g;
					g_particule_color_data[4*ParticlesCount+2] = p.b;
					g_particule_color_data[4*ParticlesCount+3] = p.a;
                } else {
                    p.cameradistance = -1.0f;
                }

                ParticlesCount++;
            }
        }

        SortParticles();

        for (int i = 0; i < ParticlesCount*4; i+=4) {
            glPushMatrix();
            glTranslatef(g_particule_position_size_data[i]/100, g_particule_position_size_data[i+1]/100, g_particule_position_size_data[i+2]/100);
            glRotatef(30, 0, 0, 0);

            glEnable(GL_BLEND);
            glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
            
            glBindTexture(GL_TEXTURE_2D, texture);
            glBegin(GL_QUADS);
                glTexCoord2f(0.0f, 0.0f); glVertex3f(-0.03f, -0.03f, 0.00f);
                glTexCoord2f(1.0f, 0.0f); glVertex3f(0.03f, -0.03f, 0.00f);
                glTexCoord2f(1.0f, 1.0f); glVertex3f(0.03f, 0.03f, 0.00f);
                glTexCoord2f(0.0f, 1.0f); glVertex3f(-0.03f, 0.03f, 0.00f);
            glEnd();
            glutSwapBuffers();
            glFlush();

            glPopMatrix();
        }
    
        usleep(10000);  
    
    } while (true);
}

int main(int argc, char *argv[]) {
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
    glutInitWindowSize(1024, 768);
    glutCreateWindow("SMT");
    glewExperimental = true;
    glewInit();

    glClearColor(0.0, 0.0, 0.0, 0.0);
    glEnable(GL_TEXTURE_2D);
    glShadeModel(GL_SMOOTH);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
    glClearDepth(1.0f);
    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

    glMatrixMode(GL_MODELVIEW);


    glutDisplayFunc(Display);
    glutMainLoop();

    return 0;
}

